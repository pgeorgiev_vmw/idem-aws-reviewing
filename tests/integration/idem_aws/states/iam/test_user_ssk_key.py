import copy
import uuid
from collections import ChainMap

import pytest
from Cryptodome.PublicKey import RSA


@pytest.mark.asyncio
async def test_user_ssh_key(hub, ctx, aws_iam_user):

    key = RSA.generate(2048)
    ssh_public_key_body = str(key.publickey().exportKey("OpenSSH").decode("utf-8"))

    user_name = aws_iam_user.get("user_name")
    ssh_public_key_id = str(uuid.uuid4())

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Create with test flag
    ret = await hub.states.aws.iam.user_ssh_key.present(
        test_ctx,
        name=ssh_public_key_id,
        user_name=user_name,
        ssh_public_key_body=ssh_public_key_body,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert (
        f"Would upload the ssh public key and attach it to iam user '{user_name}'"
        in ret["comment"]
    )

    # upload ssh public key to IAM user
    ret = await hub.states.aws.iam.user_ssh_key.present(
        ctx,
        name=ssh_public_key_id,
        user_name=user_name,
        ssh_public_key_body=ssh_public_key_body,
    )
    assert (
        f"Uploaded the ssh public key and attached it to iam user '{user_name}'"
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert "Active" == resource.get("status")
    resource_id = resource.get("resource_id")
    assert resource.get("ssh_public_key_body")

    # Describe attached user policies and verify the format of describe parameters
    describe_ret = await hub.states.aws.iam.user_ssh_key.describe(ctx)
    assert resource_id in describe_ret
    assert "aws.iam.user_ssh_key.present" in describe_ret.get(resource_id)
    described_resource = describe_ret.get(resource_id).get(
        "aws.iam.user_ssh_key.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert user_name == described_resource_map.get("user_name")
    assert "Active" == described_resource_map.get("status")
    assert ssh_public_key_body == described_resource_map.get("ssh_public_key_body")

    # Update ssh_key to Inactive with test flag
    ret = await hub.states.aws.iam.user_ssh_key.present(
        test_ctx,
        name=resource_id,
        resource_id=resource_id,
        ssh_public_key_body=ssh_public_key_body,
        user_name=user_name,
        status="Inactive",
    )
    assert (
        f"Would update ssh public key '{resource_id}' of user '{user_name}' status to Inactive"
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    assert "Inactive" == ret["new_state"].get("status")

    # Update ssh_key to Inactive
    ret = await hub.states.aws.iam.user_ssh_key.present(
        ctx,
        name=resource_id,
        resource_id=resource_id,
        ssh_public_key_body=ssh_public_key_body,
        user_name=user_name,
        status="Inactive",
    )
    assert (
        f"Updated aws.iam.user_ssh_key '{resource_id}' status to 'Inactive'"
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert "Inactive" == resource.get("status")

    # Update ssh_key to without status
    ret = await hub.states.aws.iam.user_ssh_key.present(
        ctx,
        name=resource_id,
        resource_id=resource_id,
        ssh_public_key_body=ssh_public_key_body,
        user_name=user_name,
    )

    assert ret.get("old_state") and ret.get("new_state")
    assert ret.get("old_state") == ret.get("new_state")

    ret = await hub.states.aws.iam.user_ssh_key.absent(
        test_ctx, name=resource_id, user_name=user_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert f"Would delete aws.iam.user_ssh_key '{resource_id}'" in ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    # delete the ssh public key
    ret = await hub.states.aws.iam.user_ssh_key.absent(
        ctx, name=resource_id, user_name=user_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert f"Deleted aws.iam.user_ssh_key '{resource_id}'" in ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    # delete the ssh public key again and verify it is not making a call to AWS
    # and promptly saying ssh key already absent
    ret = await hub.states.aws.iam.user_ssh_key.absent(
        ctx, name=resource_id, user_name=user_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert f"aws.iam.user_ssh_key '{resource_id}' already absent" in ret["comment"]
    assert not ret.get("old_state") and not ret.get("new_state")
