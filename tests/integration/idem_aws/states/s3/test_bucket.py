import copy
import uuid
from collections import ChainMap
from typing import Any
from typing import Dict
from typing import List

import pytest
from deepdiff import DeepDiff


@pytest.mark.asyncio
async def test_bucket(hub, ctx):
    # Create bucket
    bucket_temp_name = "idem-test-bucket-" + str(uuid.uuid4())
    tags = [{"Key": "Name", "Value": bucket_temp_name}]
    create_bucket_configuration = {"LocationConstraint": ctx.acct.region_name}
    object_ownership = "BucketOwnerEnforced"

    # Create s3 bucket with test flag "idem state --test"
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.s3.bucket.present(
        test_ctx,
        name=bucket_temp_name,
        create_bucket_configuration=create_bucket_configuration,
        object_lock_enabled_for_bucket=True,
        object_ownership=object_ownership,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert f"Would create aws.s3.bucket '{bucket_temp_name}'" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert tags == resource.get("tags")
    assert bucket_temp_name == resource.get("name")
    assert create_bucket_configuration == resource.get("create_bucket_configuration")
    assert object_ownership == resource.get("object_ownership")
    assert resource.get("object_lock_enabled_for_bucket")

    # Create s3 bucket
    ret = await hub.states.aws.s3.bucket.present(
        ctx,
        name=bucket_temp_name,
        create_bucket_configuration=create_bucket_configuration,
        object_lock_enabled_for_bucket=True,
        object_ownership=object_ownership,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert f"Created aws.s3.bucket '{bucket_temp_name}'" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    resource_id = resource.get("resource_id")
    assert bucket_temp_name == resource.get("name")
    assert create_bucket_configuration == resource.get("create_bucket_configuration")
    # Localstack pro does not populate object_ownership in the output
    if not hub.tool.utils.is_running_localstack(ctx):
        assert object_ownership == resource.get("object_ownership")
    assert resource.get("object_lock_enabled_for_bucket")
    assert "FULL_CONTROL" == resource.get("grant_full_control")

    assert not ret["changes"].get("old") and ret["changes"]["new"]
    resource = ret["changes"]["new"]

    # resource properties shall follow yaml notation
    resource_id = resource.get("name")

    new_tags = [{"Key": "Name", "Value": bucket_temp_name}]
    object_lock_configuration = (
        {
            "ObjectLockEnabled": "Enabled",
            "Rule": {
                "DefaultRetention": {
                    "Mode": "GOVERNANCE",
                    "Days": 1,
                }
            },
        },
    )
    # Update s3 bucket with test flag "idem state --test"
    ret = await hub.states.aws.s3.bucket.present(
        test_ctx,
        name=bucket_temp_name,
        resource_id=bucket_temp_name,
        create_bucket_configuration=create_bucket_configuration,
        object_lock_enabled_for_bucket=True,
        object_lock_configuration=object_lock_configuration,
        object_ownership=object_ownership,
        tags=new_tags,
    )
    assert ret["result"], ret["comment"]
    assert f"aws.s3.bucket {bucket_temp_name} already exists." in ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert new_tags == resource.get("tags")
    assert bucket_temp_name == resource.get("name")
    assert create_bucket_configuration == resource.get("create_bucket_configuration")
    if not hub.tool.utils.is_running_localstack(ctx):
        assert object_ownership == resource.get("object_ownership")
    assert resource.get("object_lock_enabled_for_bucket")
    assert "FULL_CONTROL" == resource.get("grant_full_control")

    # Update s3 bucket
    ret = await hub.states.aws.s3.bucket.present(
        ctx,
        name=bucket_temp_name,
        resource_id=bucket_temp_name,
        create_bucket_configuration=create_bucket_configuration,
        object_lock_enabled_for_bucket=True,
        object_lock_configuration=object_lock_configuration,
        object_ownership=object_ownership,
        tags=new_tags,
    )
    assert ret["result"], ret["comment"]
    assert f"aws.s3.bucket {bucket_temp_name} already exists." in ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert new_tags == resource.get("tags")
    assert bucket_temp_name == resource.get("name")
    assert create_bucket_configuration == resource.get("create_bucket_configuration")
    if not hub.tool.utils.is_running_localstack(ctx):
        assert object_ownership == resource.get("object_ownership")
    assert "FULL_CONTROL" == resource.get("grant_full_control")

    # Describe s3 bucket
    describe_ret = await hub.states.aws.s3.bucket.describe(ctx)
    assert resource_id in describe_ret
    # Verify that the describe output format is correct
    assert "aws.s3.bucket.present" in describe_ret.get(resource_id)
    described_resource = describe_ret.get(resource_id).get("aws.s3.bucket.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert "FULL_CONTROL" == described_resource_map.get("grant_full_control")
    assert create_bucket_configuration == described_resource_map.get(
        "create_bucket_configuration"
    )
    assert tags == described_resource_map.get("tags")
    assert described_resource_map.get("object_lock_enabled_for_bucket")

    # Delete s3 bucket with test flag "idem state --test"
    ret = await hub.states.aws.s3.bucket.absent(
        test_ctx, name=resource_id, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert f"Would delete aws.s3.bucket '{bucket_temp_name}'" in ret["comment"]

    # Delete s3 bucket
    ret = await hub.states.aws.s3.bucket.absent(
        ctx, name=resource_id, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert f"Deleted aws.s3.bucket '{bucket_temp_name}'" in ret["comment"]
    resource = ret.get("old_state")
    assert new_tags == resource.get("tags")
    assert bucket_temp_name == resource.get("name")
    assert create_bucket_configuration == resource.get("create_bucket_configuration")
    if not hub.tool.utils.is_running_localstack(ctx):
        assert object_ownership == resource.get("object_ownership")
    assert "FULL_CONTROL" == resource.get("grant_full_control")

    # Deleting s3 bucket again should be an no-op
    ret = await hub.states.aws.s3.bucket.absent(
        ctx, name=resource_id, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    assert f"aws.s3.bucket '{bucket_temp_name}' already absent" in ret["comment"]


@pytest.mark.asyncio
async def test_bucket_absent_with_none_resource_id(hub, ctx):
    bucket_temp_name = "idem-test-bucket-" + str(uuid.uuid4())
    # Delete s3 bucket with resource_id as None. Result in no-op.
    ret = await hub.states.aws.s3.bucket.absent(
        ctx, name=bucket_temp_name, resource_id=None
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.s3.bucket", name=bucket_temp_name
        )[0]
        in ret["comment"]
    )


@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_bucket_lifecycle_configuration_CRUD(hub, ctx):
    # Create bucket with lifecycle configuration
    lifecycle_configuration = {
        "Rules": [
            {
                "Filter": {"Prefix": "logs/"},
                "Expiration": {"Days": 3},
                "ID": "test-rule",
                "Status": "Enabled",
            }
        ]
    }

    bucket_temp_name = "idem-test-bucket-" + str(uuid.uuid4())
    ret = await hub.states.aws.s3.bucket.present(
        ctx,
        name=bucket_temp_name,
        lifecycle_configuration=lifecycle_configuration,
        create_bucket_configuration={"LocationConstraint": ctx["acct"]["region_name"]},
    )

    assert ret["result"], ret["comment"]
    assert not ret["changes"].get("old") and ret["changes"].get("new")
    assert ret["new_state"]
    assert not ret["old_state"]
    resource = ret["changes"]["new"]
    assert resource

    resource_id = resource.get("name")
    assert resource_id
    assert resource.get("lifecycle_configuration")
    assert equals(resource.get("lifecycle_configuration"), lifecycle_configuration)

    # verify lifecycle configuration created
    describe_ret = await hub.states.aws.s3.bucket.describe(ctx)

    bucket = describe_ret.get(bucket_temp_name)
    assert bucket
    assert bucket.get("aws.s3.bucket.present")

    sls_items = bucket.get("aws.s3.bucket.present")
    assert verify_lifecycle_configuration_in_sls_items(
        lifecycle_configuration, sls_items
    )

    # update s3 bucket with the same lifecycle configuration state
    ret = await hub.states.aws.s3.bucket.present(
        ctx,
        name=bucket_temp_name,
        resource_id=bucket_temp_name,
        lifecycle_configuration=lifecycle_configuration,
    )

    # verify that no mutation is actually performed
    assert ret["result"], ret["comment"]
    assert not ret["changes"].get("old") and not ret["changes"].get("new")
    assert ret["new_state"]
    assert ret["old_state"]
    assert equals(ret["new_state"], ret["old_state"])

    # mutate lifecycle config of existing bucket
    lifecycle_configuration_mutated = {
        "Rules": [
            {
                "Filter": {"Prefix": "logs/"},
                "Expiration": {"Days": 5},
                "ID": "test-rule",
                "Status": "Enabled",
                "NoncurrentVersionExpiration": {"NoncurrentDays": 10},
            }
        ]
    }

    expected_mutated_lifecycle_configuration = {
        "Rules": [
            {
                "Filter": {"Prefix": "logs/"},
                "Expiration": {"Days": 5},
                "ID": "test-rule",
                "Status": "Enabled",
                "NoncurrentVersionExpiration": {"NoncurrentDays": 10},
            }
        ]
    }

    ret = await hub.states.aws.s3.bucket.present(
        ctx,
        name=bucket_temp_name,
        resource_id=bucket_temp_name,
        lifecycle_configuration=lifecycle_configuration_mutated,
    )

    # verify the actual s3 bucket lifecycle configuration mutation
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    assert ret["old_state"].get("lifecycle_configuration")
    assert equals(
        ret["old_state"].get("lifecycle_configuration"), lifecycle_configuration
    )
    assert ret["new_state"].get("lifecycle_configuration")
    assert equals(
        ret["new_state"].get("lifecycle_configuration"),
        expected_mutated_lifecycle_configuration,
    )
    assert ret["changes"].get("old") and ret["changes"].get("new")

    # delete instance and clean up the testbed s3 bucket
    ret = await hub.states.aws.s3.bucket.absent(
        ctx, name=bucket_temp_name, resource_id=bucket_temp_name
    )
    assert ret["result"], ret["comment"]
    assert ret["changes"].get("old") and not ret["changes"].get("new")
    assert not ret["new_state"]
    assert ret["old_state"]


@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_add_lifecycle_config_to_already_existing_bucket(hub, ctx):

    # Create bucket without lifecycle configuration
    bucket_temp_name = "idem-test-bucket-" + str(uuid.uuid4())

    ret = await hub.states.aws.s3.bucket.present(
        ctx,
        name=bucket_temp_name,
        create_bucket_configuration={"LocationConstraint": ctx["acct"]["region_name"]},
    )

    assert ret["result"], ret["comment"]
    assert not ret["old_state"] and ret["new_state"]
    assert not ret["changes"].get("old") and ret["changes"].get("new")
    resource = ret["changes"]["new"]
    assert resource
    assert not resource.get("lifecycle_configuration")
    ret["changes"]["new"]

    lifecycle_configuration = {
        "Rules": [
            {
                "Filter": {"Prefix": "logs/"},
                "Expiration": {"Days": 3},
                "ID": "test-rule-new",
                "Status": "Enabled",
            }
        ]
    }

    # add a lifecycle configuration to an already existing s3 bucket item
    ret = await hub.states.aws.s3.bucket.present(
        ctx,
        name=bucket_temp_name,
        resource_id=bucket_temp_name,
        lifecycle_configuration=lifecycle_configuration,
    )

    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    assert not ret["old_state"].get("lifecycle_configuration")
    assert not ret["changes"].get("old") and ret["changes"].get("new")
    assert ret["new_state"].get("lifecycle_configuration")
    assert equals(
        ret["new_state"].get("lifecycle_configuration"), lifecycle_configuration
    )

    # clean up the s3 bucket testbed resource
    ret = await hub.states.aws.s3.bucket.absent(
        ctx, name=bucket_temp_name, resource_id=bucket_temp_name
    )

    assert ret["result"], ret["comment"]
    assert ret["changes"].get("old") and not ret["changes"].get("new")
    assert not ret["new_state"]
    assert ret["old_state"]


def equals(a: Dict[str, Any], b: Dict[str, Any]) -> bool:
    return len(DeepDiff(a, b)) == 0


def verify_lifecycle_configuration_in_sls_items(
    lifecycle_config: Dict[str, Any], items: List[Dict[str, Any]]
) -> bool:
    if not items:
        return false

    for item in items:
        if list(item.keys())[0] == "lifecycle_configuration":
            return equals(list(item.values())[0], lifecycle_config)

    return false
