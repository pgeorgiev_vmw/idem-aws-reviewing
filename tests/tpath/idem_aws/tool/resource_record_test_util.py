from typing import Dict


async def do_asserts(
    hub,
    ctx,
    hosted_zone_id: str,
    name: str,
    resource_record: Dict,
    comment: str,
    true_state: Dict,
    old_state: Dict,
    new_state: Dict,
):
    """
    Assertions used by aws.route53.resrouce_record test code
    Args:
        hub: The redistributed pop central hub.
        ctx: A dict with the keys/values for the execution of the Idem run.
        hosted_zone_id(Text): ID of the hosted zone holding the resource record/.
        name(Text): name of the resource record.
        resource_record(Dict): sls representation of the resource record that is asserted.
        comment(Text): expected comment string.
        true_state(Dict): expected resource record state as read from aws.
        old_state(Dict): expected old_state.
        new_state(Dict): expected new_state.

    """
    assert resource_record["result"], resource_record["comment"]
    lookup_record_raw = (
        await hub.tool.aws.route53.resource_record_utils.find_resource_record(
            ctx, hosted_zone_id, f"{name}.", "A"
        )
    )
    assert lookup_record_raw["result"]
    if "aws_state" in lookup_record_raw:
        current_state = hub.tool.aws.route53.conversion_utils.convert_raw_resource_record_to_present(
            hosted_zone_id, lookup_record_raw["aws_state"]
        )
        assert hub.tool.aws.route53.resource_record_utils.same_states(
            current_state, true_state
        )
    else:
        assert true_state is None
    assert comment in resource_record["comment"]
    assert hub.tool.aws.route53.resource_record_utils.same_states(
        resource_record["old_state"], old_state
    )
    assert hub.tool.aws.route53.resource_record_utils.same_states(
        resource_record["new_state"], new_state
    )
