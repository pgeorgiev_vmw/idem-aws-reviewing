from unittest import mock


class mock_session:
    args = None
    kwargs = {}

    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs


async def test_gather(hub):
    return
    profile_name = "mock_profile_1"
    id_ = "mock_id"
    key = "mock_key"
    token = "mock_token"
    region = "mock_region"
    endpoint_url = "endpoint_url"
    bogus_kwarg = "this should be ignored"

    hub.acct.PROFILES["aws.boto"] = {
        profile_name: {
            "aws_access_key_id": id_,
            "aws_secret_access_key": key,
            "aws_session_token": token,
            "region_name": region,
            "endpoint_url": endpoint_url,
            "bogus_kwarg": bogus_kwarg,
        }
    }
    with mock.patch.object(hub.tool.aws, "SESSION", mock_session):
        profiles = await hub.acct.aws.boto.gather({})

    # Verify that the profile was created
    assert profile_name in profiles
    # Verify that endpoint_url and provider_tag_key became part of the profile
    assert profiles[profile_name].endpoint_url == endpoint_url

    # verify that a boto3 session was added to the profile
    assert hasattr(profiles[profile_name], "session")

    # No unexpected args should have been passed to the session
    assert not getattr(profiles[profile_name].session, "args", None)

    # Verify that all the expected kwargs are present
    kwargs = getattr(profiles[profile_name].session, "kwargs", {})

    assert kwargs.get("aws_access_key_id") == id_
    assert kwargs.get("aws_secret_access_key") == key
    assert kwargs.get("aws_session_token") == token
    assert kwargs.get("region_name") == region

    # Verify that all the extra kwargs were consumed before they got passed to the session
    assert not kwargs.get("bogus_kwarg")
    assert not kwargs.get(
        "endpoint_url"
    ), "endpoint_url should have been consumed by the session.get contract"
