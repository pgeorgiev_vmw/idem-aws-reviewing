def test_compare_dicts(hub):

    # comparing two dicts and returning true if they are same
    data_sources_old = {"S3Logs": {"Enable": True}}
    data_sources_new = {"S3Logs": {"Enable": True}}
    compare_dicts = hub.tool.aws.guardduty.detector_utils.compare_dicts(
        data_sources_old, data_sources_new
    )
    assert compare_dicts

    # comparing two dicts and returning false if they are not same
    data_sources_old = {"S3Logs": {"Enable": True}}
    data_sources_new = {"S3Logs": {"Enable": False}}
    compare_dicts = hub.tool.aws.guardduty.detector_utils.compare_dicts(
        data_sources_old, data_sources_new
    )
    assert not compare_dicts


def test_render_data_sources(hub):

    # rendering data_sources to make resources similar
    before_render = {"S3Logs": {"Status": "ENABLED"}}
    after_render = {"S3Logs": {"Enable": True}}
    render_result = hub.tool.aws.guardduty.detector_utils.render_data_sources(
        before_render
    )
    compare_dicts = hub.tool.aws.guardduty.detector_utils.compare_dicts(
        render_result, after_render
    )
    assert compare_dicts

    before_render = {"Kubernetes": {"AuditLogs": {"Status": "ENABLED"}}}
    after_render = {"Kubernetes": {"AuditLogs": {"Enable": True}}}
    render_result = hub.tool.aws.guardduty.detector_utils.render_data_sources(
        before_render
    )
    compare_dicts = hub.tool.aws.guardduty.detector_utils.compare_dicts(
        render_result, after_render
    )
    assert compare_dicts
