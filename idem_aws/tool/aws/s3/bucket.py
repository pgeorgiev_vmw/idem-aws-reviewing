from typing import Any
from typing import Dict

from deepdiff import DeepDiff


async def update_lifecycle_configuration(
    hub, ctx, resource_id, lifecycle_configuration: Dict[str, Any]
):
    """
    Convenient util method for creating a lifecycle configuration on s3 bucket without existing
    lifecycle configuration or updating of already defined s3 bucket lifecycle configuration.
    Args:
        hub: The redistributed pop central hub.
        ctx: A dict with the keys/values for the execution of the Idem run located in `hub.idem.RUNS[ctx['run_name']]`.
        resource_id: s3 bucket resource id
        lifecycle_configuration: a dictionary which is defining the lifecycle configuration spec

    Returns:
        {"result": True|False, "comment": A message Tuple, "ret": None, "resource_updated": is s3 bucket actually mutated, "plan_state": mutation planning state }
    """

    result = dict(
        comment=(), result=True, ret=None, resource_updated=False, plan_state={}
    )

    if not lifecycle_configuration:
        return result

    actual_lifecycle_config = (
        await hub.exec.boto3.client.s3.get_bucket_lifecycle_configuration(
            ctx=ctx, Bucket=resource_id
        )
    )

    if not actual_lifecycle_config["result"]:
        # create lifecycle configuration to the cloud entity

        lifecycle_config_create_ret = None

        if not ctx.get("test", False):
            lifecycle_config_create_ret = (
                await hub.exec.boto3.client.s3.put_bucket_lifecycle_configuration(
                    ctx=ctx,
                    Bucket=resource_id,
                    LifecycleConfiguration=lifecycle_configuration,
                )
            )

            if ctx.get("test", False):
                result["result"] = True
                result["plan_state"][
                    "lifecycle_configuration"
                ] = lifecycle_configuration
            else:
                result["ret"] = lifecycle_config_create_ret["ret"]
                result["result"] = (
                    result["result"] and lifecycle_config_create_ret["result"]
                )
                if lifecycle_config_create_ret["result"]:
                    result["resource_updated"] = True
                    result["comment"] += hub.tool.aws.comment_utils.create_comment(
                        resource_type="aws.s3.bucket.lifecycle_configuration",
                        name=resource_id,
                    )
                else:
                    result["resource_updated"] = False
                    result["comment"] += (
                        "Lifecycle configuration is not applied to the s3 bucket with name: "
                        + resource_id,
                    )

    else:
        # update case
        actual_config_rules_list = actual_lifecycle_config.get("ret").get("Rules")

        diff = DeepDiff(
            lifecycle_configuration.get("Rules"),
            actual_config_rules_list,
            ignore_order=True,
            ignore_order_func=ignore_order_func,
        )

        if not diff:
            result["resource_updated"] = False

        else:
            config_list_mutation_spec = (
                hub.tool.aws.s3.conversion_utils.generate_lifecycle_rules_mutation_list(
                    lifecycle_configuration.get("Rules"),
                    actual_config_rules_list,
                )
            )

            lifecycle_config_spec = {"Rules": config_list_mutation_spec}

            lifecycle_config_mutation = None

            if not ctx.get("test", False):
                lifecycle_config_mutation = (
                    await hub.exec.boto3.client.s3.put_bucket_lifecycle_configuration(
                        ctx=ctx,
                        Bucket=resource_id,
                        LifecycleConfiguration=lifecycle_config_spec,
                    )
                )

            if ctx.get("test", False):
                result["result"] = True
                result["plan_state"][
                    "lifecycle_configuration"
                ] = lifecycle_configuration
            else:

                result["result"] = (
                    result["result"] and lifecycle_config_mutation["result"]
                )
                result["ret"] = lifecycle_config_mutation["ret"]

                if lifecycle_config_mutation["result"]:
                    result["resource_updated"] = True
                    result["comment"] += (
                        "Lifecycle configuration has been successfully applied to the s3 bucket with name: "
                        + resource_id,
                    )
                else:
                    result["resource_updated"] = False
                    result["comment"] += (
                        "Lifecycle configuration is not update for s3 bucket with name: "
                        + resource_id,
                    )

    return result


def ignore_order_func(level):
    return "a" in level.path()
