import json
from typing import Any
from typing import Dict
from typing import List


def convert_raw_bucket_notification_to_present(
    hub, raw_resource: Dict[str, Any], bucket_name: str
) -> Dict[str, Any]:
    """
    Convert the s3 bucket notification configurations response to a common format

    Args:
        raw_resource: Dictionary of s3 bucket notification configurations
        bucket_name: Name of the bucket on which notification needs to be configured.

    Returns:
        A dictionary of s3 bucket notification configurations
    """
    translated_resource = {}
    raw_resource.pop("ResponseMetadata", None)

    if raw_resource:
        translated_resource["name"] = bucket_name
        translated_resource["resource_id"] = bucket_name + "-notifications"
        translated_resource["notifications"] = json.loads(json.dumps(raw_resource))
    return translated_resource


async def convert_raw_s3_to_present(
    hub, ctx, idem_resource_name: str = None
) -> Dict[str, Any]:
    """
    Convert the s3 bucket configurations response to a common format

    Args:
        idem_resource_name: Name of the bucket.

    Returns:
        Dict[str, Any]
    """
    resource_translated = {
        "name": idem_resource_name,
        "resource_id": idem_resource_name,
    }
    result = dict(comment=(), result=True, ret=None)
    ret = await hub.exec.boto3.client.s3.get_object_lock_configuration(
        ctx, Bucket=idem_resource_name
    )
    if not ret["result"]:
        result["comment"] = result["comment"] + ret["comment"]
    if ret["result"]:
        if ret["ret"]["ObjectLockConfiguration"]["ObjectLockEnabled"] == "Enabled":
            resource_translated["object_lock_enabled_for_bucket"] = True
        else:
            resource_translated["object_lock_enabled_for_bucket"] = False
    ret = await hub.exec.boto3.client.s3.get_bucket_location(
        ctx, Bucket=idem_resource_name
    )
    if not ret["result"]:
        result["comment"] = result["comment"] + ret["comment"]
    if ret["result"] and ret["ret"]["LocationConstraint"]:
        resource_translated["create_bucket_configuration"] = {
            "LocationConstraint": ret["ret"]["LocationConstraint"]
        }
    ret = await hub.exec.boto3.client.s3.get_bucket_ownership_controls(
        ctx, Bucket=idem_resource_name
    )
    if not ret["result"]:
        result["comment"] = result["comment"] + ret["comment"]
    if (
        ret["result"]
        and ret["ret"]["OwnershipControls"]
        and ret["ret"]["OwnershipControls"]["Rules"][0]["ObjectOwnership"]
    ):
        resource_translated["object_ownership"] = ret["ret"]["OwnershipControls"][
            "Rules"
        ][0]["ObjectOwnership"]
    ret = await hub.exec.boto3.client.s3.get_bucket_acl(ctx, Bucket=idem_resource_name)
    if not ret["result"]:
        result["comment"] = result["comment"] + ret["comment"]
    if ret["result"] and ret["ret"]["Grants"][0]["Permission"]:
        if ret["ret"]["Grants"][0]["Permission"] == "FULL_CONTROL":
            resource_translated["grant_full_control"] = "FULL_CONTROL"
        elif ret["ret"]["Grants"][0]["Permission"] == "READ":
            resource_translated["grant_read"] = "READ"
        elif ret["ret"]["Grants"][0]["Permission"] == "READ_ACP":
            resource_translated["grant_read_acp"] = "READ_ACP"
        elif ret["ret"]["Grants"][0]["Permission"] == "WRITE":
            resource_translated["grant_write"] = "WRITE"
        else:
            resource_translated["grant_write_acp"] = "WRITE_ACP"

    ret = await hub.exec.boto3.client.s3.get_bucket_tagging(
        ctx, Bucket=idem_resource_name
    )
    if not ret["result"]:
        result["comment"] = result["comment"] + ret["comment"]
    if ret["result"] and ret["ret"]["TagSet"]:
        resource_translated["tags"] = ret["ret"]["TagSet"]

    lifecycle_configuration_ret = (
        await hub.exec.boto3.client.s3.get_bucket_lifecycle_configuration(
            ctx=ctx, Bucket=idem_resource_name
        )
    )

    if not lifecycle_configuration_ret["result"]:
        result["comment"] += (lifecycle_configuration_ret["comment"],)
    if lifecycle_configuration_ret["result"] and lifecycle_configuration_ret["ret"]:
        resource_translated[
            "lifecycle_configuration"
        ] = convert_lifecycle_configuration_to_present(
            hub, config=lifecycle_configuration_ret["ret"]
        )

    result["ret"] = resource_translated
    return result


def convert_raw_public_access_block_to_present(
    hub, bucket_name: str, public_access_block_configuration: Dict[str, Any]
) -> Dict[str, Any]:
    translated_resource = {
        "name": f"{bucket_name}-public-access-block",
        "bucket": bucket_name,
        "public_access_block_configuration": json.loads(
            json.dumps(public_access_block_configuration)
        ),
        "resource_id": bucket_name,
    }
    return translated_resource


def convert_raw_bucket_policy_to_present(
    hub, raw_resource: Dict[str, Any], bucket: str, name: str
) -> Dict[str, Any]:
    """
    Util function to convert the s3 bucket policy response to a common format

    Args:
        hub: required for functions in hub
        raw_resource: S3 bucket policy response
        bucket: Name of the bucket on which policy needs to be configured.

    Returns:
        A dictionary of s3 bucket notification configurations
    """
    translated_resource = {}
    raw_resource.pop("ResponseMetadata", None)

    if raw_resource:
        translated_resource["name"] = name
        translated_resource["resource_id"] = bucket + "-policy"
        translated_resource["bucket"] = bucket
        translated_resource["policy"] = raw_resource["Policy"]
    return translated_resource


def convert_lifecycle_configuration_to_present(
    hub,
    config: Dict[str, Any],
) -> Dict[str, Any]:
    """
    Util function to convert the s3 bucket lifecycle configuration to a common format

    Args:
        hub: required for functions in hub
        config: S3 bucket lifecycle configuration spec

    Returns:
        A dictionary of s3 bucket lifecycle configuration definition
    """

    res = {}

    if not config:
        return res

    rules = config.get("Rules")

    if not rules:
        return res

    res["Rules"] = rules

    return res


def generate_lifecycle_rules_mutation_list(
    hub,
    desired_rules_list: List[Dict[str, Any]],
    actual_rules_list: List[Dict[str, Any]],
) -> List[Dict[str, Any]]:
    """
    Util function to generate lifecycle configuration rules mutation list.

    There lifecycle configuration rules mutation set contains three subsets:
     - rules defined in the cloud and not mutated in the SLS present spec, those rules shall not be updated or removed – they shall be intact.
     - rules defined in the SLS spec and not existing in the cloud state, those rules shall be created in the AWS cloud state.
     - rules that exist in the AWS cloud and are mutated in the SLS state definition,
      in that case the SLS present state shall override the existing AWS cloud configuration

    The effective mutation request spec is a union of the three subsets enumerated above.

    Args:
        hub: required for functions in hub,
        desired_rules_list: list of the desired lifecycle rules,
        actual_rules_list: list of the actual current lifecycle rules,

    Returns:
        A list of s3 bucket lifecycle configuration rules.
    """

    if not desired_rules_list or not actual_rules_list:
        return {}

    desired_rules_ids = [rule["ID"] for rule in desired_rules_list]
    desired_rules_with_ids_dict = dict(zip(desired_rules_ids, desired_rules_list))
    actual_rules_ids = [rule["ID"] for rule in actual_rules_list]
    actual_rules_with_ids_dict = dict(zip(actual_rules_ids, actual_rules_list))

    desired_rules_ids_set = set(desired_rules_ids)
    actual_rules_ids_set = set(actual_rules_ids)

    intersection_desired_and_actual_ids_set = actual_rules_ids_set.intersection(
        desired_rules_ids
    )
    desired_non_colliding_lifecycle_configuration_ids_set = (
        desired_rules_ids_set - intersection_desired_and_actual_ids_set
    )
    actual_non_colliding_lifecycle_configuration_ids_set = (
        actual_rules_ids_set - intersection_desired_and_actual_ids_set
    )

    colliding_rules_list = []

    for id in intersection_desired_and_actual_ids_set:
        # desired state configuration is over-riding the actual state in case of collision
        colliding_rules_list.append(
            merge(
                desired_rules_with_ids_dict.get(id), actual_rules_with_ids_dict.get(id)
            )
        )

    desired_non_colliding_rules_list = []

    for id in desired_non_colliding_lifecycle_configuration_ids_set:
        desired_non_colliding_rules_list.append(desired_rules_with_ids_dict.get(id))

    actual_non_colliding_lifecycle_configuration_rules_list = []

    for id in actual_non_colliding_lifecycle_configuration_ids_set:
        actual_non_colliding_lifecycle_configuration_rules_list.append(
            actual_rules_with_ids_dict.get(id)
        )

    return (
        colliding_rules_list
        + desired_non_colliding_rules_list
        + actual_non_colliding_lifecycle_configuration_rules_list
    )


def merge(dict1: Dict[str, Any], dict2: Dict[str, Any]):
    """
    Util function to merge two dictionary items.
    Note that in case of collision the colliding item in the first dictionary
    actual parameter is overriding the second dictionary parameter.
    It shall be mentioned that the util function is supporting
    merging nested dictionary structures.

    Args:
        dict1: first Dict structure
        dict2: second Dict structure
    Returns:
        A merged dictionary that contains all non-colliding items of dict1 and dict2 items and for colliding items dict1
        are overriding the dict2 elements.
    """

    res = {}
    for k in set(dict1.keys()).union(dict2.keys()):
        if k in dict1 and k in dict2:
            if isinstance(dict1[k], dict) and isinstance(dict2[k], dict):
                res[k] = merge(dict1[k], dict2[k])
            else:
                # Value from first dict overrides one in first and we move on.
                res[k] = dict1[k]
        elif k in dict1:
            res[k] = dict1[k]
        else:
            res[k] = dict2[k]

    return res
